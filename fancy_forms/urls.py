"""fancy_forms URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from forms.views import collection_detail, collection_detail_data, collection_list

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', collection_list),
    path(r'collection/<int:collection_pk>', collection_detail, name='detail_collection'),
    path(r'collection-data/<int:collection_data_pk>',  collection_detail_data, name='detail_collection_data'),
]
