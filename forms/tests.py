from django.test import TestCase

from forms.helper import to_json
from forms.models import Collection, Field, FieldLink, FieldData, CollectionData, CollectionLink
from forms.views import create_root_collection


class CollectionDataConverterCase(TestCase):
    def setUp(self):
        self.input_dict = {
            'Profile%Year': '2017',
            'Profile%Address%Street': 'Ahornstrasse',
            'Profile%Address%Number': '3e',
            'Profile%University%Name': 'TUM',
            'Profile%University%Address%Street': 'Arcisstrasse',
            'Profile%University%Address%Number': '21',
        }

        self.target_dict = {
            'Profile': {
                'Year': '2017',
                'Address': {
                    'Street': 'Ahornstrasse',
                    'Number': '3e',
                },
                'University': {
                    'Name': 'TUM',
                    'Address': {
                        'Street': 'Arcisstrasse',
                        'Number': '21',
                    }
                }
            }
        }

    def test_to_json(self):
        result = to_json(self.input_dict)
        self.assertEquals(to_json(result), self.target_dict)


class CollectionSave(TestCase):
    def setUp(self):
        self.c = Collection.objects.create(name='Profile')
        f = Field.objects.create(name='Year')
        fl = FieldLink.objects.create(name='Year of Birth', top=self.c, bottom=f)

    def test_save(self):
        year = '1993'

        data_dict = {
            'Profile': {
                'Year of Birth': year,
            }
        }

        create_root_collection(data_dict, None)

        fd_set = FieldData.objects.all()
        fd = fd_set.first()
        cd_set = CollectionData.objects.all()
        cd = cd_set.last()

        self.assertEquals(len(fd_set), 1)
        self.assertEquals(fd.field_data, year)
        self.assertEquals(fd.collection_data.id, cd.id)
        self.assertEquals(len(cd_set), 1)


class CollectionSaveRecursive(TestCase):
    def setUp(self):
        self.c = Collection.objects.create(name='Profile')
        f = Field.objects.create(name='Year')
        fl = FieldLink.objects.create(name='Year of Birth', top=self.c, bottom=f)
        cl = CollectionLink.objects.create(name='ChildProfile', top=self.c, bottom=self.c)

    def test_save(self):
        year = '1993'
        data_dict = {
            'Profile': {
                'Year of Birth': year,
                'ChildProfile': {
                    'Year of Birth': year,
                }
            }
        }


        create_root_collection(data_dict, None)

        fd_set = FieldData.objects.all()
        fd = fd_set.first()
        cd_set = CollectionData.objects.all()
        cd = cd_set.first()

        self.assertEquals(cd.fielddata_set.all().count(), 1)

        self.assertEquals(len(fd_set), 2)
        self.assertEquals(fd.field_data, year)
        self.assertEquals(fd.collection_data.id, cd.id)
        self.assertEquals(len(cd_set), 2)
