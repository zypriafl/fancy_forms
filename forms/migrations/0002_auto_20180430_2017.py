# Generated by Django 2.0.4 on 2018-04-30 20:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('forms', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='collectionlink',
            name='name',
            field=models.CharField(blank=True, max_length=128),
        ),
        migrations.AlterField(
            model_name='fieldlink',
            name='name',
            field=models.CharField(blank=True, max_length=128),
        ),
    ]
