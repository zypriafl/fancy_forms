from django.shortcuts import render, get_object_or_404, redirect

from forms.helper import to_json
from forms.models import Collection, CollectionLink, FieldLink, CollectionData, FieldData
from copy import deepcopy


def collection_list(request):
    collection_set = Collection.objects.filter(linked_parent_collections__isnull=True)
    context = {'collection_set': collection_set}

    return render(request, 'list_collections.html', context)


def collection_detail(request, collection_pk):
    if request.POST:
        return collection_save_or_update(request, collection_pk)

    c = Collection.objects.get(id=collection_pk)
    cd_set = CollectionData.objects.filter(collection=c, is_root=True)

    context = {'content': collection_to_html(c, None),
               'cd_set': cd_set,
               'collection_pk': collection_pk}

    return render(request, 'detail_collection.html', context)


def collection_detail_data(request, collection_data_pk):
    cd = CollectionData.objects.get(id=collection_data_pk)
    if request.POST:
        return collection_save_or_update(request, cd.collection.id)

    cd = get_object_or_404(CollectionData, id=collection_data_pk, is_root=True)
    c = cd.collection
    cd_set = CollectionData.objects.filter(collection=c, is_root=True)

    context = {'content': collection_to_html(c, cd),
               'cd_set': cd_set,
               'collection_data_pk': collection_data_pk}

    return render(request, 'detail_collection_data.html', context)


def collection_save_or_update(request, collection_pk):
    c = Collection.objects.get(id=collection_pk)
    cd = CollectionData.objects.filter(collection=c, is_root=True).last()

    if 'update' in request.POST:
        form_dict = deepcopy(request.POST)
        form_dict.pop('csrfmiddlewaretoken')
        form_dict.pop('update')
        form_json = to_json(form_dict)

        collection_data = update_root_collection(form_json, None, cd)

    elif 'create' in request.POST:
        form_dict = deepcopy(request.POST)
        form_dict.pop('csrfmiddlewaretoken')
        form_dict.pop('create')
        form_json = to_json(form_dict)

        collection_data = create_root_collection(form_json, None)

    else:
        raise ValueError("Add 'create' or 'update' key to form dict.")

    return redirect(collection_data)


def update_root_collection(data_dict, parent_collection_data, initial_data=False):
    for key, value in data_dict.items():
        if isinstance(value, dict):
            try:
                # Try to get next Collection via CollectionLink
                cl = CollectionLink.objects.get(name=key)
                c = cl.bottom
                is_root = False
            except CollectionLink.DoesNotExist:
                # Root Collection does not have a incoming CollectionLink
                c = Collection.objects.get(name=key)
                cl = None
                is_root = True

            # Create CollectionData for Collection
            if initial_data:
                cd = initial_data  #
            else:
                cd = CollectionData.objects.get(collection=c, is_root=is_root, collection_link=cl,
                                                parent_collection_data=parent_collection_data)

            # Set parent for new created CollectionData.
            if parent_collection_data:
                cd.parent_collection_data = parent_collection_data
                cd.save()

            update_root_collection(value, cd)
        else:
            field_link = FieldLink.objects.get(name=key)
            field_data = FieldData.objects.get(field_link=field_link, collection_data=parent_collection_data)
            field_data.field_data = value
            field_data.save()

    return parent_collection_data or cd


def create_root_collection(data_dict, parent_collection_data):
    for key, value in data_dict.items():
        if isinstance(value, dict):
            try:
                # Try to get next Collection via CollectionLink
                cl = CollectionLink.objects.get(name=key)
                c = cl.bottom
                is_root = False
            except CollectionLink.DoesNotExist:
                # Root Collection does not have a incoming CollectionLink
                c = Collection.objects.get(name=key)
                cl = None
                is_root = True

            # Create CollectionData for Collection
            cd = CollectionData.objects.create(collection=c, is_root=is_root, collection_link=cl)

            # Set parent for new created CollectionData.
            if parent_collection_data:
                cd.parent_collection_data = parent_collection_data
                cd.save()

            create_root_collection(value, cd)
        else:
            field_link = FieldLink.objects.get(name=key)
            FieldData.objects.create(field_data=value, field_link=field_link, collection_data=parent_collection_data)

    return parent_collection_data or cd


def _get_collection(collection_or_collection_link):
    if isinstance(collection_or_collection_link, CollectionLink):
        collection = collection_or_collection_link.bottom

    elif isinstance(collection_or_collection_link, Collection):
        collection = collection_or_collection_link

    else:
        raise ValueError('Must be Collection or CollectionLink')

    assert isinstance(collection, Collection)

    return collection


def collection_to_html(collection_or_collection_link, collection_data, path=None):
    collection = _get_collection(collection_or_collection_link)

    if not path:
        path = collection_or_collection_link.name
    else:
        path += '%' + collection_or_collection_link.name

    collection_links = collection.linked_child_collections.all()
    field_links = collection.linked_child_fields.all()

    collection_data_set = CollectionData.objects.filter(
        parent_collection_data=collection_data,
        collection_link__in=collection.linked_child_collections.all()
    )
    field_data_set = collection_data.fielddata_set.all() if collection_data else []

    assert collection_data_set.count() <= 1

    return "<div><h2>{}:</h2>".format(collection_or_collection_link.name) \
           + form_to_html(field_links, field_data_set, path=path) + '<hr>' \
           + '\n'.join(
        [collection_to_html(x, collection_data_set.first(), path=path) for x in collection_links]) + '</div>'


def form_to_html(field_links, field_data_set, path):
    path_format = '{}%{}'
    html_field_list = []

    for i, x in enumerate(field_links):
        html_field_list.append(
            "<label for='{}'>{}: </label>"
            "<input id='{}' type='text' name='{}' value='{}'>"
            "<br>"
            .format(
                path_format.format(path, x.name),
                path_format.format(path, x.name),
                path_format.format(path, x.name),
                path_format.format(path, x.name),
                field_data_set[i].field_data if len(field_data_set) > i else ''
            )
        )

    return '\n'.join(html_field_list)

# Example Form
# <form action="/your-name/" method="post">

#     <label for="your_name">Your name: </label>
#     <input id="your_name" type="text" name="your_name" value="{{ current_name }}">

#     <input type="submit" value="OK">
# </form>
