from django.db import models


class Field(models.Model):
    name = models.CharField(max_length=128)  # e.g. Name

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Collection(models.Model):
    name = models.CharField(max_length=128)  # e.g. Address

    collections = models.ManyToManyField('self', through='CollectionLink', symmetrical=False)
    fields = models.ManyToManyField(Field, through='FieldLink')

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('name',)


# Linking Model
class FieldLink(models.Model):
    top = models.ForeignKey(Collection, on_delete=models.CASCADE, related_name='linked_child_fields')
    bottom = models.ForeignKey(Field, on_delete=models.CASCADE, related_name='linked_parent_collections')

    name = models.CharField(max_length=128, blank=True, null=False)  # e.g. Private Address, Study Address, ...
    position = models.IntegerField(default=0)

    # Cardinality: '0' = at most once,, '1' = exactly once  '+' = at least once, '*' = no restriction
    cardinality = models.CharField(max_length=1, default='*')

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('position',)

    def __str__(self):
        return 'FieldLink: {}'.format(self.name)


class CollectionLink(models.Model):
    top = models.ForeignKey(Collection, on_delete=models.CASCADE, related_name='linked_child_collections')
    bottom = models.ForeignKey(Collection, on_delete=models.CASCADE, related_name='linked_parent_collections')

    name = models.CharField(max_length=128, blank=True, null=False)  # e.g. Private Address, Study Address, ...
    position = models.IntegerField(default=0)

    # Cardinality: '0' = at most once,, '1' = exactly once  '+' = at least once, '*' = no restriction
    cardinality = models.CharField(max_length=1, default='*')

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('position',)

    def __str__(self):
        return 'CollectionLink: {}'.format(self.name)


# Data
class CollectionData(models.Model):
    collection = models.ForeignKey(Collection, on_delete=models.CASCADE)
    parent_collection_data = models.ForeignKey('self', null=True, blank=True, on_delete=models.CASCADE)

    collection_link = models.ForeignKey(CollectionLink, null=True, on_delete=models.CASCADE)

    is_root = models.BooleanField(default=False, null=False, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def get_absolute_url(self):
        from django.urls import reverse
        return reverse('detail_collection_data', args=[str(self.id)])

    def __str__(self):
        return "Data for {}".format(str(self.collection))


class FieldData(models.Model):
    field_data = models.CharField(max_length=1000)
    collection_data = models.ForeignKey(CollectionData, on_delete=models.CASCADE)

    field_link = models.ForeignKey(FieldLink, null=True, on_delete=models.CASCADE)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "FieldData containing: {}".format(self.field_data)

# From Django Docs, ManyToManyField with through (https://docs.djangoproject.com/en/2.0/topics/db/models/)

# from django.db import models
#
# class Person(models.Model):
#     name = models.CharField(max_length=128)
#
#     def __str__(self):
#         return self.name
#
# class Group(models.Model):
#     name = models.CharField(max_length=128)
#     members = models.ManyToManyField(Person, through='Membership')
#
#     def __str__(self):
#         return self.name
#
# class Membership(models.Model):
#     person = models.ForeignKey(Person, on_delete=models.CASCADE)
#     group = models.ForeignKey(Group, on_delete=models.CASCADE)
#     date_joined = models.DateField()
#     invite_reason = models.CharField(max_length=64)
