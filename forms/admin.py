from django.contrib import admin

# Register your models here.
from forms.models import Field, Collection, CollectionLink, FieldLink, FieldData, CollectionData


class CollectionLinkInline(admin.TabularInline):
    model = CollectionLink
    extra = 1
    fk_name = 'top'


class FieldLinkInline(admin.TabularInline):
    model = FieldLink
    extra = 1


class CollectionAdmin(admin.ModelAdmin):
    inlines = (CollectionLinkInline, FieldLinkInline,)


admin.site.register(Field)
admin.site.register(Collection, CollectionAdmin)


# Data Admins


class FieldDataInline(admin.TabularInline):
    model = FieldData
    extra = 1


class CollectionDataInline(admin.TabularInline):
    model = CollectionData
    extra = 1


class CollectionDataAdmin(admin.ModelAdmin):
    inlines = (FieldDataInline,)
    list_display = ('__str__', 'id', 'is_root', 'created_at', 'collection_link')
    # exclude = ('collection_datas',)


class FieldDataAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'field_data', 'created_at', 'field_link')


admin.site.register(FieldData, FieldDataAdmin)
admin.site.register(CollectionData, CollectionDataAdmin)

# Django Docs (https://docs.djangoproject.com/en/dev/ref/contrib/admin/#working-with-many-to-many-intermediary-models)

# class TeachSubjectInline(admin.TabularInline):
#     model = TeachSubject
#     extra = 2 # how many rows to show
#
# class SchoolClassAdmin(admin.ModelAdmin):
#     inlines = (TeachSubjectInline,)
#
# admin.site.register(SchoolClass, SchoolClassAdmin)
