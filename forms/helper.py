from copy import deepcopy


def to_json(data_dict):
    result_dict = {}

    for top_key in get_top_keys(data_dict):
        result_dict[top_key] = {}
        new_dict_1 = filter_dict(data_dict, top_key)
        new_dict = strip_dict(new_dict_1)
        if new_dict:
            result_dict[top_key].update(to_json(new_dict))
        else:
            result_dict[top_key] = data_dict[top_key]

    return result_dict


def get_top_keys(data_dict):
    top_keys = []
    for key in data_dict.keys():
        if '%' in key:
            top_key, remainder = key.split('%', 1)
            top_keys.append(top_key)
        else:
            top_keys.append(key)

    return set(top_keys)


def strip_dict(data_dict):
    stripped_data_dict = {}

    for key, value in data_dict.items():
        if '%' in key:
            top_key, remainder = key.split('%', 1)
            stripped_data_dict[remainder] = value

    return stripped_data_dict


def filter_dict(data_dict, top_key):
    data_dict_copy = deepcopy(data_dict)

    for key in data_dict.keys():
        if not key.startswith(top_key):
            data_dict_copy.pop(key)

    return data_dict_copy
